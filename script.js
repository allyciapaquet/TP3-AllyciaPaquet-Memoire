// JEU DE MEMOIRE
'use strict'

const nbrePaires = document.getElementById('paires')
const joueur = document.getElementById('joueur')
const jeu = document.getElementById('jeu')
const alphanum = /^[a-zA-Z0-9]+$/ // chaîne alphanumérique
const intervale = demarrerTimer(5)
const formulaire = document.getElementById('form')
formulaire.addEventListener('submit', valider)
let cartePareil = document.getElementsByClassName('pareil')
let cartesTournees = new Array()
var pairesTrouvees = 0

let cartesMelangees = [];
// utilisation de l'objet
let infoJeu = {
    nomJeu: ['jeu de mémoire'],
    type: ['jeu de carte']
}

console.log(`Ceci est un ${infoJeu.nomJeu} et c'est un ${infoJeu.type}.`)

// Valider mon formulaire & créer le jeu 



/**
 *
 * @description valider le nom du joueur et le nombre de paires
 * @param {string} 
 * @returns message si pas valide
 */
function valider(e) {
    let messages = []//messages pour les erreurs de validation

    if (joueur.value.length <= 0 || nbrePaires.value.length <= 0) { // si champs remplis
        messages.push('Vous devez remplir les champs demander')
    }

    if (alphanum.test(joueur.value) === false) { //si alphanumérique
        messages.push('Utiliser uniquement des caractères alphanumériques ')
    }
    if (nbrePaires.value < 2 || nbrePaires.value > 10) { //paire entre 2 et 10
        messages.push("Le nombre de paire doit etre entre 2 et 10")
    }
    if (messages.length > 0) { //si pas valide, ne pas valider le formulaire
        e.preventDefault()
        const ul = document.getElementById('messages')
        ul.innerHTML = ""
        for (let i = 0; i < messages.length; i++) {
            const li = document.createElement('li')
            const texte = document.createTextNode(messages[i])
            li.appendChild(texte)
            ul.appendChild(li)
        }
    } else { // si valider creer le jeu & boutons 
        e.preventDefault()
        let section = formulaire
        cacherJeu(section)
        creerBoutons()
        // const intervale = demarrerTimer(5)
        document.getElementById('commencer').innerHTML = 'Trouver tous les paires en moins de 5 minutes !'
    }
}


function cacherJeu(section) {
    section.style.display = 'none'
}



/**
 *
 * @description creer tableau pour les cartes
 * @param {array}
 * @return les cartes dans le tableau
 */
function leTableauCartes() {
    const nbreTours = nbrePaires
    const tableauCartes = []
    for (let i = 0; i < nbreTours.value; i++) {
        tableauCartes.push(i)
        tableauCartes.push(i)
    }
    return tableauCartes
}


// Creation des cartes


/**
 *
 * @description creer les boutons qui seront les cartes, creer index qui permet de verifier si les cartes sont pareil ou non
 * @return {number} nombre de bouton selon nbre de paire choisis, action selon sils sont pareil ou non 
 */
function creerBoutons() {
    cartesMelangees = tableauCartesMelangees()
    jeu.innerHTML = ""
    for (let i = 0; i < cartesMelangees.length; i++) {
        let cartes = document.createElement('button')
        cartes.innerHTML = ""
        cartes.style.height = '150px'
        cartes.style.width = '100px'
        cartes.style.margin = '10px'
        cartes.id = i;
        cartes.setAttribute('data-nombre-cache', i)
        cartes.onclick = function () { afficherNombreDeLaCarte(i); };
        jeu.appendChild(cartes)

        const index = cartesMelangees[i]
        cartes.setAttribute('nombre-cache', index)
        cartes.addEventListener('click', clicCartes => {
		
        cartesTournees.push(index)
        cartes.textContent = index
		
        cartes.disabled = true
		//ajouter une classe pour signifier que la carte est sélectionnée
        cartes.classList.add('selectionnee')
        if (cartesTournees.length === 2) {
            let carte1 = cartesTournees[0]
            let carte2 = cartesTournees[1]
            if (carte1 === carte2) {				
				//"get" la pair de carte selectionner
                let memeCarte = document.getElementsByClassName('selectionnee')
				//ajouter une classe pour signifier que les cartes sélectionnées sont une paire 
                memeCarte[0].classList.add('pareil')
                memeCarte[1].classList.add('pareil')
				memeCarte[0].classList.remove('selectionnee')
                memeCarte[0].classList.remove('selectionnee')
                memeCarte.textContent = index                   
				//ajouter la paire trouvée au compteur global 
                pairesTrouvees = pairesTrouvees + 1
				
				//vérifier si toutes les paires ont été trouvés
				if(pairesTrouvees * 2 === cartesMelangees.length){
					alert("Victoire")
				}
            } else {				
				//enlever la classe de selection, le texte et le disable
				let pasMemeCartes = document.getElementsByClassName('selectionnee')
				setTimeout(function(){						
					pasMemeCartes[0].disabled = false
					pasMemeCartes[1].disabled = false
					pasMemeCartes[0].textContent = ''
					pasMemeCartes[1].textContent = ''
					pasMemeCartes[0].classList.remove('selectionnee')
					pasMemeCartes[0].classList.remove('selectionnee')
				}, 1000) // 1 secondes 					
            }
            cartesTournees.pop()
            cartesTournees.pop()
        }

        });
    }
    return cartesMelangees
}



/**
 *
 *
 * @description mettre un chiffre aleatoire sur les cartes
 */
function afficherNombreDeLaCarte(id) {
    var laCarte = document.getElementById(id);
    laCarte.innerHTML = cartesMelangees[id];
}



/**
 *
 *
 * @return {array} tableau avec les cartes melangees
 */
function tableauCartesMelangees() {
    const tableauCartes = leTableauCartes()
    const tableauCartesMelangees = []
    while (tableauCartes.length > 0) {
        const index = Math.floor(Math.random() * tableauCartes.length)
        tableauCartesMelangees.push(tableauCartes[index])
        tableauCartes.splice(index, 1)
        console.log(tableauCartes)
        console.log(tableauCartesMelangees)
    }
    return tableauCartesMelangees
}


// Timer 5 minutes




/**
 *
 *
 * @param {number} nbreMinutes
 * @return {array} tableau avec les cartes melangees
 */

function demarrerTimer(nbreMinutes) {
    const temps = nbreMinutes * 60
    MiseAjour(temps)
    const intervale = setInterval(temps5Minutes, 1000)
    return intervale
}



function temps5Minutes() {
    let messages = []
    let nbreSecondes = document.getElementById('timer').getAttribute('data-secondes')
    nbreSecondes = parseInt(nbreSecondes)
    nbreSecondes = nbreSecondes - 1
    MiseAjour(nbreSecondes)

    if (nbreSecondes === 10) {
        messages.push("il reste 10 secondes")
    }
    if (nbreSecondes === 0) {
        clearInterval(intervale)
        messages.push("Le temps est écoulé")
    }
}



function MiseAjour(nbreSecondes) {
    let secondes = nbreSecondes % 60
    const minutes = Math.floor(nbreSecondes / 60)
    if (secondes < 10) {
        secondes = "0" + secondes
    }
    const timer = document.getElementById('timer')
    timer.textContent = minutes + ":" + secondes
    document.getElementById('timer').setAttribute('data-secondes', nbreSecondes)
}


